//
//  ListBookViewController.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/24/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class ListBookViewController: BaseViewController ,UITableViewDelegate, UITableViewDataSource {
    var listBook = [BookModel]()
    @IBOutlet weak var tblBook: UITableView!
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listBook.append(BookModel(textVN: "Sơ cấp", textJP: ""))
        listBook.append(BookModel(textVN: "Sơ trung cấp", textJP: ""))
        listBook.append(BookModel(textVN: "Trung cấp", textJP: ""))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listBook.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tblBook.dequeueReusableCellWithIdentifier("BookTableViewCell") as! BookTableViewCell
        cell.img.image = UIImage.init(named: String(format: "Book%d", indexPath.row))
        cell.lblTop.text = listBook[indexPath.row].textJP
        cell.lblBot.text = listBook[indexPath.row].textVN
        
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tblBook.deselectRowAtIndexPath(indexPath, animated: true)
        var transition = CATransition()
        transition.duration = 0.25
        //transition.timingFunction = timeFunc
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        let dest = self.storyboard?.instantiateViewControllerWithIdentifier("DetailLessonViewController") as! DetailLessonViewController
                
       // dest.lblNameBook.text = String(format: "Quyển \(indexPath.row): abc")
        if indexPath.row == 0 {
            self.navigationController?.pushViewController(dest, animated: true)
        }else {
            self.showPopUp("Sorry. This book is unavaible")
        }
        
   //     let loadingView = self.storyboard?.instantiateViewControllerWithIdentifier("loadingview")
        //self.presentView
    }
    
    
}
