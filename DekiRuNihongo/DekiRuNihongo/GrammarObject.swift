//
//  GrammarObject.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class GrammarObject: NSObject {
    var listGrammar : [GrammarElement]?
    init(grammar : Grammar) {
        listGrammar = [GrammarElement]()
        for item in grammar.words {
            listGrammar?.append(GrammarElement(word: item.word, meaning: item.meaning))
        }
    }
}
