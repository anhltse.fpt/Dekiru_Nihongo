//
//  TuVungViewController.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/28/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit
import AVFoundation

class TuVungViewController: UIViewController {
    var listWord = [Word]()
    //var listMean = [String]()
    let synth = AVSpeechSynthesizer()
    var myUtterance = AVSpeechUtterance(string: "")
    @IBOutlet weak var tbv: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tbv.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TuVungViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (listWord.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentify = "TuVungCell"
        
        let cell = tbv.dequeueReusableCellWithIdentifier(cellIdentify, forIndexPath: indexPath) as! TuVungCell
        //cell.imageView?.image = UIImage(named: NSString(format: "icon%d", indexPath.row) as String)
        //cell.jpWord.text = listWord[indexPath.row]
        //cell.vnWord.text = listMean[indexPath.row]
        cell.jpWord.text = listWord[indexPath.row].word
        cell.vnWord.text = listWord[indexPath.row].meaning
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tbv.deselectRowAtIndexPath(indexPath, animated: true)
        myUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        myUtterance = AVSpeechUtterance(string: listWord[indexPath.row].word)
        myUtterance.rate = 0.3
        
        synth.speakUtterance(myUtterance)
    }
}

