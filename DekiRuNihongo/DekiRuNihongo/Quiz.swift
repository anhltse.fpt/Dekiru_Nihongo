//
//  Quiz.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Quiz: Object {
    var words = List<QuizModel>()
    var number = ""
    func setValue(words : [JSON]){
        for word in words {
            let wordModel = Word(json: word)
            let wordData = QuizModel()
            wordData.setValue(wordModel.word, meaning: wordModel.meaning)
            self.words.append(wordData)
        }
       
    }
}
