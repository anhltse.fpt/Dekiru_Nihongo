//
//  BookDB.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import Foundation
import RealmSwift

class BookDB: Object {
    dynamic var number = ""
    var lessons = List<Lesson>()
}
