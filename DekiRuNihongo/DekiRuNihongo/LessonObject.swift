//
//  LessonObject.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class LessonObject: NSObject {
    var name : String = ""
    var index : String = ""
    var grammar : GrammarObject?
    var vocab : VocabObject?
    var kanji : KanJiObject?
}
