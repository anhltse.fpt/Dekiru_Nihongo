//
//  GrammarModel.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import Foundation
import RealmSwift

class GrammarModel: Object {
    dynamic var word = ""
    dynamic var meaning = ""
    
    func setValue(word : String, meaning : String){
        self.word = word
        self.meaning = meaning
    }

}
