//
//  LessonInsideViewController.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/26/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit
import CarbonKit

class LessonInsideViewController: UIViewController {
    
    @IBOutlet weak var segmentViewController: UIView!
    var carbonTabSwipeNaviGation : CarbonTabSwipeNavigation?
    var lessonObject : LessonObject?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var items: [String]?
        
        
        items = ["TỪ VỰNG", "NGỮ PHÁP", "KANJI", "KIỂM TRA"]
        
        carbonTabSwipeNaviGation = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonTabSwipeNaviGation?.insertIntoRootViewController(self, andTargetView: segmentViewController)
        
        style()

    }
    
    func style() {
        let color = UIColor(red: 24.0 / 255, green: 75.0 / 255, blue: 152.0 / 255, alpha: 1)
        
        carbonTabSwipeNaviGation?.toolbar.translucent = false
        carbonTabSwipeNaviGation?.setIndicatorColor(color)
        carbonTabSwipeNaviGation?.setTabExtraWidth(30)
        let sizeWidth = self.view.frame.width
        
        carbonTabSwipeNaviGation?.carbonSegmentedControl?.setWidth(sizeWidth / 4, forSegmentAtIndex: 0)
        carbonTabSwipeNaviGation?.carbonSegmentedControl?.setWidth(sizeWidth / 4, forSegmentAtIndex: 1)
        carbonTabSwipeNaviGation?.carbonSegmentedControl?.setWidth(sizeWidth / 4, forSegmentAtIndex: 2)
        carbonTabSwipeNaviGation?.carbonSegmentedControl?.setWidth(sizeWidth / 4, forSegmentAtIndex: 3)
        
        carbonTabSwipeNaviGation?.setNormalColor(color.colorWithAlphaComponent(0.6), font: UIFont.boldSystemFontOfSize(14))
        
        carbonTabSwipeNaviGation?.setSelectedColor(color, font: UIFont.boldSystemFontOfSize(14))
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackTouchUpInside(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}

extension LessonInsideViewController: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index {
        case 0:
            let dest =  self.storyboard?.instantiateViewControllerWithIdentifier("TuVungViewController") as! TuVungViewController
            dest.listWord = (lessonObject?.vocab?.words)!
            return dest
        case 1:
            let dest = self.storyboard?.instantiateViewControllerWithIdentifier("NguPhapViewController") as! NguPhapViewController
            var tmpList = [String]()
            for item in (lessonObject?.grammar?.listGrammar)! {
                tmpList.append(item.word!)
            }
            dest.list = tmpList
            return dest
        case 2:
            return self.storyboard?.instantiateViewControllerWithIdentifier("KanJiViewController") as! KanJiViewController
        case 3:
            return self.storyboard?.instantiateViewControllerWithIdentifier("TestViewController") as! TestViewController            
        default:
            return self.storyboard?.instantiateViewControllerWithIdentifier("KanJiViewController") as! KanJiViewController
        }
    }
}
