//
//  NguPhapViewController.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/28/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class NguPhapViewController: UIViewController {
    @IBOutlet weak var tbv : UITableView!
    var list : [String]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NguPhapViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentify = "NguPhapCell"
        
        let cell = tbv.dequeueReusableCellWithIdentifier(cellIdentify, forIndexPath: indexPath) as! NguPhapCell
        
        cell.lblShow.text = list![indexPath.row]
//        cell.jpWord.text = list[indexPath.row].word
//        cell.vnWord.text = listWord[indexPath.row].meaning
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (list?.count)!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tbv.deselectRowAtIndexPath(indexPath, animated: true)
        
        let dest = self.storyboard?.instantiateViewControllerWithIdentifier("DetailNguPhapViewController") as! DetailNguPhapViewController
        
        self.navigationController?.pushViewController(dest, animated: true)
        //self.navigationController?.pushViewController(dest!, animated: true)
        
    }
}