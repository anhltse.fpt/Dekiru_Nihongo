//
//  Lesson.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import Foundation
import RealmSwift

class Lesson: Object {
    dynamic var title = ""
    dynamic var vocab : Vocab? = nil
    dynamic var grammar : Grammar? = nil
    dynamic var kanji : KanJi? = nil
    dynamic var quiz : Quiz? = nil
}
