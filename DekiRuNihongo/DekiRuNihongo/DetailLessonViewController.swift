//
//  DetailLessonViewController.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/26/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit
import RealmSwift

class DetailLessonViewController: UIViewController {
    var listLesson = [LessonObject]()
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblNameBook: UILabel!
    @IBOutlet weak var tblDetail: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        let book1 = realm.objects(BookDB.self)
        let book = book1[0]
        var index = 0
        for model in book.lessons {
            index = index + 1
            let lessonObj = LessonObject()
            lessonObj.index = String(format: "%d", index)
            lessonObj.name = model.title
            lessonObj.vocab = VocabObject(vocab: model.vocab!)
            if model.grammar != nil {
                lessonObj.grammar = GrammarObject(grammar: model.grammar!)
            }            
            listLesson.append(lessonObj)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackTouchUpInside(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

}

extension DetailLessonViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (listLesson.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tblDetail.dequeueReusableCellWithIdentifier("LessonCellTableViewCell") as! LessonCellTableViewCell
        cell.img.image = UIImage.init(named: String(format: "icon%d", indexPath.row + 1))
        cell.title.text = listLesson[indexPath.row].name
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tblDetail.deselectRowAtIndexPath(indexPath, animated: true)
        var transition = CATransition()
        transition.duration = 0.25
        //transition.timingFunction = timeFunc
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        let dest = self.storyboard?.instantiateViewControllerWithIdentifier("LessonInsideViewController") as! LessonInsideViewController
       dest.lessonObject = listLesson[indexPath.row]
        
        self.navigationController?.pushViewController(dest, animated: true)

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
}

extension UIImage {
    var rounded: UIImage? {
        let imageView = UIImageView(image: self)
        imageView.layer.cornerRadius = min(size.height/2, size.width/2)
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.renderInContext(context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    var circle: UIImage? {
        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .ScaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.renderInContext(context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}
