//
//  TuVungCell.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/29/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class TuVungCell: UITableViewCell {

    @IBOutlet weak var jpWord: UILabel!
    @IBOutlet weak var vnWord: UILabel!
   
}
