//
//  Word.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/29/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit
import SwiftyJSON
class Word: NSObject {
    var word : String!
    var meaning : String!
    
    init(word : String, meaning : String) {
            self.word = word
            self.meaning = meaning
    }
    
    init(json : JSON) {
        self.word = json["n"].stringValue
        self.meaning = json["m"].stringValue
    }
}
