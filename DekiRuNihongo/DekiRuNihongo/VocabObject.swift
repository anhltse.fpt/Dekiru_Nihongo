//
//  VocabObject.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class VocabObject: NSObject {
    var words = [Word]()
    init( vocab : Vocab) {
        for model in vocab.words {
            words.append(Word(word: model.word, meaning: model.meaning))
        }
    }
}
