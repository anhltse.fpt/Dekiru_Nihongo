//
//  Config.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class Config: NSObject {
    let BOOK1_NAME_JP = ""
    let BOOK2_NAME_JP = ""
    let BOOK3_NAME_JP = ""
    let BOOK1_NAME_VN = "Sơ cấp"
    let BOOK2_NAME_VN = "Sơ trung cấp"
    let BOOK3_NAME_VN = "Trung cấp"
}
