//
//  LoadingViewController.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 5/23/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Realm
import RealmSwift

class LoadingViewController: BaseViewController {
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    static func convertStringToDictionary(json: String) {
        
        var formattedString = json;
        formattedString.removeAtIndex(formattedString.characters.startIndex);
        formattedString = formattedString.substringToIndex(formattedString.endIndex.predecessor())
        formattedString = formattedString.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        )
        if let data = formattedString.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                
                let book = BookDB()
                book.number = "1"
                let json = JSON(data: data)
                //title
                let titles = json["data"]["list1"]["b1"].arrayValue
                for model in titles {
                    let lessonTmp = Lesson()
                    lessonTmp.title = model.stringValue
                    book.lessons.append(lessonTmp)
                }
                
                //vocab
                for i in 1 ..< 16 {
                    let tmp = String(format: "l%d", i)
                    let words = json["data"]["vocab1"][tmp].arrayValue
                    let object = Vocab()
                    object.number = String(format: "%d", i)
                    object.setValue(words)
                    book.lessons[i-1].vocab = object
                }
                //gra
                for i in 1 ..< 4 {
                    let tmp = String(format: "l%d", i)
                    let grammar = json["data"]["gra1"][tmp].arrayValue
                    let object = Grammar()
                    object.number = String(format: "%d", i)
                    object.setValue(grammar)
                    book.lessons[i-1].grammar = object
                }
                //quiz 
                for i in 1 ..< 4 {
                    let tmp = String(format: "l%d", i)
                    let grammar = json["data"]["quiz1"][tmp].arrayValue
                    let object = Quiz()
                    object.number = String(format: "%d", i)
                    object.setValue(grammar)
                    book.lessons[i-1].quiz = object
                }
                //kanji
                for i in 1 ..< 3 {
                    let tmp = String(format: "l%d", i)
                    let grammar = json["data"]["kan1"][tmp].arrayValue
                    let object = KanJi()
                    object.number = String(format: "%d", i)
                    object.setValue(grammar)
                    book.lessons[i-1].kanji = object
                }                
                let realm = try! Realm()
                try! realm.write({ 
                    realm.add(book)
                })
            } catch let error as NSError {
                NSLog("a \(error)")
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
