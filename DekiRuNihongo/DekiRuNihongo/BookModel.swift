//
//  BookModel.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class BookModel: NSObject {
    var textVN : String
    var textJP : String
    init(textVN : String, textJP : String){
        self.textJP = textJP
        self.textVN = textVN
    }
}
