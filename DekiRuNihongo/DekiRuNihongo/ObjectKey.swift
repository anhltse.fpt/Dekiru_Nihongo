//
//  ObjectKey.swift
//  DekiRuNihongo
//
//  Created by Anh Tuan on 8/25/16.
//  Copyright © 2016 Anh Tuan. All rights reserved.
//

import UIKit

class ObjectKey: NSObject {
    var word : String = ""
    var meaning : String = ""
    init(word : String , meaning : String) {
        self.word = word
        self.meaning = meaning
    }
}
